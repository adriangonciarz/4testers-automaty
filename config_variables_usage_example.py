from config import THRESHOLD


def send_slack_notification(value):
    print(f"Slack message: alert was triggered for value {value} after exceeding the threshold of {THRESHOLD}")


def trigger_alert(value):
    print(f"Alert triggered for value {value}!")


def check_if_alert_should_be_triggered(value):
    if value > THRESHOLD:
        trigger_alert(value)
        send_slack_notification(value)


if __name__ == '__main__':
    check_if_alert_should_be_triggered(0.3)
    check_if_alert_should_be_triggered(0.5)
    check_if_alert_should_be_triggered(0.9)
