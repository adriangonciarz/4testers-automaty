import random
import time

import config
from consultation_utils import foo, bar

my_pet_name = "Panela"
pets = []


def foo_bar():
    my_name = "Adrian"
    my_email = "ag@example.com"
    my_adress = "asdadasds"

    print(f"Start foobar {my_name}")
    foo()
    print("hello from the app!")
    bar()
    print(f"End foobar {my_name}")


def login_as_admin():
    print(f"Login {config.admin_email}, Password: {config.admin_password}")


def generate_pet_dictionary():
    return {
        "name": f"Azor {time.time()}",
        "kind": f"dog {random.random()}"
    }


def add_new_pet():
    pets.append(generate_pet_dictionary())


def add_three_pets():
    for i in range(3):
        add_new_pet()


def add_five_pets():
    for i in range(5):
        add_new_pet()


def add_ten_pets():
    for i in range(10):
        add_new_pet()


list_of_transformations = [
    add_three_pets,
    add_five_pets,
    add_ten_pets
]

if __name__ == '__main__':
    pets = ["ab", "ba", "cd", "cd",  "ddd", "fgh", "eee"]
    chosen_pet = pets.pop(-2)
    print(chosen_pet)
    print(pets)

    index_of_cd = pets.index("cd")
    print(index_of_cd)
    pets.reverse()
    print(pets)
    pets.insert(3, "foobar")

    new_pets = ["bird", "cat", "dog"]

    pets.extend(new_pets)
    print(pets)