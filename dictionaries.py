def prepare_user_payload(email, phone, city, street):
    return {
        'contact': {
            'email': email,
            'phone': phone
        },
        'address': {
            'city': city,
            'street': street
        }
    }

if __name__ == '__main__':
    pet = {
        "name": "Panela",
        "kind": "dog",
        "age": 2,
        "weight": 8.6,
        "is_male": False,
        "favourite_food": ["chicken", "fish", "trash"]
    }

    print(pet["weight"])
    pet["weight"] = 8.8
    pet["likes_swimming"] = False
    del pet["is_male"]
    pet["favourite_food"].append("snacks")
    print(pet)

    payload = prepare_user_payload("aaaa@example.com", "+48001001001", "Kraków", "Długa 13/9")
    print(payload)