def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(area_of_a_circle)


def upper_word(word):
    return word.upper()


def add_two_numbers(a, b):
    return a + b


def calculate_square_of_number(number):
    return number ** 2


def calculate_cuboid_volume(a, b, c):
    return a * b * c


def convert_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 9 / 5 + 32


def print_welcome_sentence(name, city):
    print(f"Witaj {name.capitalize()}! Miło Cię widzieć w naszym mieście: {city.capitalize()}!")


if __name__ == '__main__':
    big_dog = upper_word("dog")
    print(big_dog)

    print(upper_word("cat"))
    print(upper_word("myszka"))

    print(add_two_numbers(3, 5))
    print(add_two_numbers("kot", "pies"))
    print(add_two_numbers(False, False))
    print(add_two_numbers(3, 3.5))

    print(calculate_square_of_number(0))
    print(calculate_square_of_number(2.55))

    print(calculate_cuboid_volume(3, 5, 7))

    print(convert_celsius_to_fahrenheit(20))
    print_welcome_sentence("adrian", "gdynia")
