def print_player_description(player_dict):
    source = player_dict.get("source", "Steam")
    print(f"The player {player_dict["nick"]} is of type {player_dict["type"]} and has {player_dict["exp_points"]} EXP")
    print(f"The source of player is {source}")


def validate_animal_data(age, kind):
    if age < 0:
        raise ValueError("Age must be greater than or equal to zero")
    elif kind not in ["dog", "cat"]:
        raise ValueError("Only dogs and cats are supported")


def is_animal_qualified_for_vaccination(age, kind):
    return (kind == "dog" and (age - 1) % 2 == 0) or (kind == "cat" and (age - 1) % 3 == 0)


def check_if_animal_requires_vaccination(age, kind):
    validate_animal_data(age, kind)
    return is_animal_qualified_for_vaccination(age, kind)


if __name__ == '__main__':
    game_player = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

    game_player_2 = {
        "nick": "kolo_365",
        "type": "magician",
        "exp_points": 2000,
        "source": "Playstation"
    }

    print_player_description(game_player)
    print_player_description(game_player_2)

    print("Cat, 1:", check_if_animal_requires_vaccination(1, "cat"))
    print("Dog, 1:", check_if_animal_requires_vaccination(1, "dog"))
    print("Cat, 2:", check_if_animal_requires_vaccination(2, "cat"))
    print("Cat, 4:", check_if_animal_requires_vaccination(4, "cat"))
    print("Dog, 3:", check_if_animal_requires_vaccination(3, "dog"))
    print("Dog, 2:", check_if_animal_requires_vaccination(2, "dog"))
