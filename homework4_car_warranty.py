from datetime import date


def car_has_warranty(production_year, mileage):
    age = date.today().year - production_year
    return age <= 5 and mileage <= 60_000


def reverse_list(input_list):
    print(input_list, id(input_list))
    reversed = input_list[::-1]
    print(reversed, id(reversed))
    print(id(input_list[::-1]))
    sublist = input_list[1:3]
    print(sublist, id(sublist))
    input_list.reverse()
    print(input_list, id(input_list))


def reference_or_copy(input_list):
    second_list = input_list
    third_list = input_list.copy()
    print(input_list, id(input_list))
    print(second_list, id(input_list))
    print(third_list, id(third_list))
    second_list.append(7)
    third_list.append(9)
    print(input_list)
    print(second_list)
    print(third_list)


if __name__ == '__main__':
    reference_or_copy([1, 2, 3, 4, 5])
