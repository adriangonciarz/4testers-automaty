def describe_list_of_emails(emails):
    print("First element of the list is:", emails[0])
    print("Last element of the list is:", emails[-1])
    emails.append("das@example.com")
    print("Currently the list is:", emails)


def create_email_in_domain_4testers(first_name, last_name):
    return f"{first_name.lower()}.{last_name.lower()}@4testers.pl"


def get_average_grade(list_of_grades):
    return sum(list_of_grades) / len(list_of_grades)


def create_email_for_user_in_domain(prefix, domain="gmail.com"):
    return f"{prefix}@{domain}"


def generate_user_contact_data_dictionary(email, phone_number, country="Poland", country_code="+48"):
    return {
        "email": email,
        "phone_number": f"{country_code}{phone_number}",
        "country": country
    }


if __name__ == '__main__':
    describe_list_of_emails(["a@example.com", "b@example.com"])

    print(create_email_in_domain_4testers("Janusz", "NOWAK"))
    print(create_email_in_domain_4testers("Barbara", "Kowalska"))

    grades = [5, 4, 4, 2, 3, 5, 2, 4, 3, 3]
    print(get_average_grade(grades))

    print(5 / 5)
    print(create_email_for_user_in_domain("adrian123", "onet.pl"))
    print(create_email_for_user_in_domain("adrian123"))

    print("pies", "kot", "żaba", sep=";")

    print(generate_user_contact_data_dictionary("adrian@example.com", "678678678", country_code="+41"))