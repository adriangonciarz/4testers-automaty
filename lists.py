def get_three_highest_numbers_in_the_list(list_of_numbers):
    # sorted_list = sorted(list_of_numbers, reverse=True)
    # return sorted_list[0:3]
    sorted_list = sorted(list_of_numbers)
    return sorted_list[-3:]


if __name__ == '__main__':
    movies = ["Dune", "Blade Runner", "Solaris", "Lost Highway", "Star Wars"]

    print(len(movies))
    print(movies[0])
    last_movie_index = len(movies) - 1
    print(movies[-1])

    movies.append("Her")
    movies.insert(2, "Interstellar")
    movies.append("Five o'clock tea")
    print(movies)

    print(get_three_highest_numbers_in_the_list([-9, 1, -8, 9, 11, 3, 5, 170]))
