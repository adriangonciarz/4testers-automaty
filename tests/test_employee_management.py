import employee_management
from assertpy import assert_that


def test_generate_employee_dictionary():
    employee_dictionary = employee_management.generate_employee_dictionary()
    assert sorted(["email", "seniority_years", "female"]) == sorted(employee_dictionary.keys())


def test_two_generated_employee_dictionaries_are_different():
    employee_1 = employee_management.generate_employee_dictionary()
    employee_2 = employee_management.generate_employee_dictionary()
    assert assert_that(employee_1).is_not_equal_to(employee_2)


def test_generating_list_of_employees():
    employees = employee_management.generate_list_of_employees(10)
    assert len(employees) == 10


def test_filtering_list_of_employees_for_female():
    input_list = [
        {'email': 'cc62d03b-9fd7-4995-b3a3-5106d9cf7e40@example.com', 'seniority_years': 49, 'female': False},
        {'email': '6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com', 'seniority_years': 40, 'female': True},
        {'email': '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com', 'seniority_years': 40, 'female': True},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 52, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 3


def test_filtering_list_of_male_only_employees_for_female():
    input_list = [
        {'email': 'cc62d03b-9fd7-4995-b3a3-5106d9cf7e40@example.com', 'seniority_years': 49, 'female': False},
        {'email': '6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com', 'seniority_years': 40, 'female': False},
        {'email': '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com', 'seniority_years': 40, 'female': False},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 52, 'female': False}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 0


def test_filtering_list_of_female_only_employees_for_female():
    input_list = [
        {'email': 'cc62d03b-9fd7-4995-b3a3-5106d9cf7e40@example.com', 'seniority_years': 49, 'female': True},
        {'email': '6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com', 'seniority_years': 40, 'female': True},
        {'email': '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com', 'seniority_years': 40, 'female': True},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 52, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list)
    assert len(female_employees) == 4


def test_filtering_list_of_employees_for_male():
    input_list = [
        {'email': 'cc62d03b-9fd7-4995-b3a3-5106d9cf7e40@example.com', 'seniority_years': 49, 'female': False},
        {'email': '6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com', 'seniority_years': 40, 'female': True},
        {'email': '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com', 'seniority_years': 40, 'female': False},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 52, 'female': True},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 52, 'female': True}
    ]
    female_employees = employee_management.filter_list_of_employees_by_gender(input_list, female=False)
    assert len(female_employees) == 2


def test_filtering_list_of_employees_for_seniority():
    input_list = [
        {'email': 'cc62d03b-9fd7-4995-b3a3-5106d9cf7e40@example.com', 'seniority_years': 2, 'female': False},
        {'email': '6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com', 'seniority_years': 8, 'female': True},
        {'email': '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com', 'seniority_years': 11, 'female': False},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 3, 'female': True},
        {'email': '813d1017-5c91-4d4e-a593-e6a4a505eb85@example.com', 'seniority_years': 1, 'female': True}
    ]
    senior_emails = employee_management.filter_list_of_employees_by_seniority_greater_than(input_list, 7)
    assert senior_emails == ['6ebe1175-6ec7-4697-93ef-fe2d201bb7a4@example.com',
                             '9bc58622-c706-4069-9f14-bb487fbc81f3@example.com']
