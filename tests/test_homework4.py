import pytest

from homework4_car_warranty import car_has_warranty


@pytest.mark.parametrize("year,mileage,expected", [
    (2023, 30000, True), (2023, 60001, False), (2018, 30000, False), (2018, 60001, False)
])
def test_car_warranty_status(year, mileage, expected):
    assert car_has_warranty(year, mileage) == expected
