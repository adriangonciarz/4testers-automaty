class User:
    def __init__(self, email, password):
        self.email = email
        self.__password = password
        self.__is_logged_in = False

    def login(self, password):
        if password == self.__password:
            self.__is_logged_in = True

    def is_logged_in(self):
        return self.__is_logged_in

    def __str__(self):
        return f"User {self.email} login status: {self.__is_logged_in}"


if __name__ == '__main__':
    user1 = User("foo@example.com", "123Password@@")
    print(user1.email)
    user1.login("123Password@3@")
    print(user1)
    user1.login("123Password@@")
    print(user1)
    user_string = str(user1)
    print(user_string)
