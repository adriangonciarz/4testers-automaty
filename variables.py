friends_name = "Michał"
friends_age = 36
friends_pet_amount = 0
has_driving_license = True
friendship_years = 10.5

print("Name:", friends_name, sep="\n")
print("Age:", friends_age, sep="\n")
print("Pet amount:", friends_pet_amount, sep="\n")
print("Has driving license:", has_driving_license, sep="\n")
print("Friendship years:", friendship_years, sep="\n")

print("---------------------------------")

print(
    "Name:", friends_name,
    "Age:", friends_age,
    "Pet amount:", friends_pet_amount,
    sep="\n"
)

name_surname = "Adrian Gonciarz"
email = "adrian.gonciarz@gmail.com"
phone_number = "+4800000123"

bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone_number
print(bio)

bio_smarter = f"Name: {name_surname}\nEmail: {email}\nPhone: {phone_number}"
print(bio_smarter)

bio_docstring = f"""Name: {name_surname}
Email: {email}
Phone: {phone_number}"""
print(bio_docstring)

physics_lecture = f"The energy of an object with mass 1g is equal to  {round(0.001*300_000_000**2)} eV"
print(physics_lecture)